# autoscaling

https://spotinst.com/blog/collecting-kubernetes-pod-metrics-heapster-is-dead/


1 
vi /etc/kubernetes/manifests/kube-controller-manager.yaml
--horizontal-pod-autoscaler-use-rest-clients=false

2 metric server

3 configuração


vi metrics-server/deploy/1.8+/metrics-server-deployment.yaml
- command:
         - /metrics-server
         - --metric-resolution=30s
         - --kubelet-insecure-tls
         #- --kubelet-preferred-address-types=InternalIP
         - --kubelet-preferred-address-types=InternalIP,Hostname,InternalDNS,ExternalDNS,ExternalIP


4 helm 

helm install stable/heapster --namespace kube-system --name heapster --set image.tag=v1.5.1 --set rbac.create=true


5 

kubectl run php-apache --image=k8s.gcr.io/hpa-example --requests=cpu=200m --expose --port=80

6

kubectl autoscale deployment php-apache --cpu-percent=50 --min=1 --max=10



